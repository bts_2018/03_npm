$( document ).ready(function() {
  
	//get HTML reference 
	var password = $('#password');
	var verifypassword = $('#verifypassword');
	var error = $('#error');
	var success = $('#success');
	var country = $('#country');
	var myForm = $('#myForm');


  	Init();
 
  	// INIT FUNCTION;
	function Init(){

		success.hide();
		error.hide();

		//AJAX LOAD DATA
		$.ajax({  
	        url: 'json/countries.json', 
	        dataType: 'json',
	        success: function (items) { 
	            $.each(items.countries, function(key, val) {
					country.append('<option value="' + val.code + '">' + val.name + '</option>');
				})
	        }
    	});

	}


	// ON CLICK EVENT
	myForm.on("submit", function(){

		
		if (password.val() != verifypassword.val()) {
			error.show();	    
			//return false;  	
		}else{
			error.hide();
			myForm.hide();
			success.show();
			//return true; 
		}

		return false; 

 	});


});

 

 